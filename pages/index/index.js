//index.js
//获取应用实例
const app = getApp()
const Api = require('../../utils/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasSignIn: false,
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    programList: [],
    isVote: false,
    pass: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options &&options.pass) {
      this.setData({
        pass: options.pass
      })
    }
    this.login();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 获取节目列表
  getShowList: function () {
    this.isHasVote()
    let that = this;
    wx.request({
      url: Api.API_HOST + 'vote',
      method: 'GET',
      success: function(res) {
        that.setData({
          programList: res.data.data
        })
      },
      fail: function(res) {},
    })
  },

  // 投票
  goToVote: function (event) {
    let that = this;
    let name = event.target.dataset.teamname;
    let userInfo = wx.getStorageSync('userInfo')
    let openid = wx.getStorageSync('openid')
    let session_key = wx.getStorageSync('session_key')
    
    wx.showModal({
      title: '提示',
      content: '您确定为' + name + '投票吗？',
      success: function (res) {
        if (res.confirm) {
          wx.request({
            url: Api.API_HOST + 'vote/canvote',
            method: 'GET',
            success: function (res) {
              if (res.data.data.canvote) {
                wx.request({
                  url: Api.API_HOST + 'vote',
                  method: 'POST',
                  data: {
                    session_key: session_key,
                    openid: openid,
                    showId: event.target.dataset.id
                  },
                  success: function (res) {
                    if (res.data.state) {
                      wx.showToast({
                        title: "投票成功！",
                        icon: 'success',
                        duration: 2000
                      })
                      that.getShowList();
                      that.setData({
                        isVote: true
                      })
                    }
                  },
                  fail: function (res) { },
                })
              } else {
                wx.showToast({
                  title: "投票暂未开启！",
                  icon: 'none',
                  duration: 3000
                })
              }
            }
          })
        }
      }
    })
  },

  // 登录
  login: function () {
    let that = this;
    wx.login({
      success: function (res) {
        if (res.code) {
          let userInfo = {};
          userInfo.code = res.code;
          wx.request({
            url: Api.API_HOST + 'users',
            method: 'POST',
            data: {
              userInfo: userInfo
            },
            success: function (res) {
              // console.log(res.data)
              wx.setStorageSync("session_key", res.data.data.sessionKey)
              wx.setStorageSync("openid", res.data.data.openid)
              // 判断是否签到了
              that.isHasSignin()
            }
          })
        }
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },

  // 判断是否已经投过票
  isHasVote: function () {
    let that = this;
    let openid = wx.getStorageSync('openid')

    wx.request({
      url: Api.API_HOST + 'vote/hasvote',
      data: {
        openid: openid
      },
      method: 'POST',
      success: function (res) {
        that.setData({
          isVote: !res.data.state
        })
      }
    })  
  },

  // 判断是否已经签到了
  isHasSignin: function () {
    let that = this;
    let openid = wx.getStorageSync('openid')
    let session_key = wx.getStorageSync('session_key')
    
    wx.request({
      url: Api.API_HOST + 'signin/hassignin',
      data: {
        openid: openid,
        session_key: session_key
      },
      method: 'POST',
      success: function (res) {
        wx.setStorageSync("hasSignin", res.data.state); // 记录签到状态
        that.handleSignin()
      }
    })
  },

  // 对用户签到状态进行处理
  handleSignin: function () {
    let that = this;
    let temp = '';
    let hasSignin = wx.getStorageSync('hasSignin')
    let userInfo = wx.getStorageSync('userInfo')
    if (that.data.pass) {
      temp = '?pass=' + that.data.pass
    }
    if (!hasSignin) { //如果还没签到,则跳转
      wx.redirectTo({
        url: '/pages/sign/sign' + temp
      })
    } else if (!userInfo) { // 如果已经签到，但是本地不存在userInfo缓存
      wx.getSetting({
        success: function (res) {
          if (res.authSetting['scope.userInfo']) {
            // 已经授权，可以直接调用 getUserInfo 获取头像昵称
            wx.getUserInfo({
              success: function (res) {
                wx.setStorageSync("userInfo", res.userInfo)
              }
            })
            // 在已经签到、获得授权的情况下，即可加载节目列表
            that.getShowList()
          } else { // 引导到签到页面,点击授权获取userInfo
            wx.redirectTo({
              url: '/pages/sign/sign' + temp
            })
          }
        }
      })
    } else {
      // 在已经签到、获得授权的情况下，即可加载节目列表
      that.getShowList()
    }
  }


})
