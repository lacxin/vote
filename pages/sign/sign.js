// pages/sign/sign.js
const app = getApp()
const Api = require('../../utils/api.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    pass: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options && options.pass) {
      this.setData({
        pass: options.pass
      })
    }
    let that = this;
    wx.login({
      success: function (res) {
        if (res.code) {
          let userInfo = {};
          userInfo.code = res.code;
          wx.request({
            url: Api.API_HOST + 'users',
            method: 'POST',
            data: {
              userInfo: userInfo
            },
            success: function (res) {
              // console.log(res.data)
              wx.setStorageSync("session_key", res.data.data.sessionKey)
              wx.setStorageSync("openid", res.data.data.openid)
            }
          })
        }
      },
      fail: function (res) { },
      complete: function (res) { },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  // 获取个人资料
  getUserInfo: function (e) {
    let that = this;
    wx.getUserInfo({
      success: function (res) {
        wx.setStorageSync("userInfo", res.userInfo)
        // 更新服务器的userInfo
        that.updateUserInfo(res.userInfo)
      }
    })
  },
  // 上传个人资料
  updateUserInfo: function (userInfo) {
    let that = this;
    let hasSignin = wx.getStorageSync('hasSignin')
    let openid = wx.getStorageSync('openid');

    wx.request({
      url: Api.API_HOST + 'users/update',
      method: 'POST',
      data: {
        userInfo: userInfo,
        openid: openid
      },
      success: function (res) {
        if (!hasSignin) {
          if (that.data.pass) {
            let openid = wx.getStorageSync('openid')
            let session_key = wx.getStorageSync('session_key')
            wx.request({
              url: Api.API_HOST + 'signin',
              data: {
                session_key: session_key,
                openid: openid,
                pass: that.data.pass
              },
              method: 'POST',
              success: function (res) {
                if (res.data.state) {
                  wx.setStorage({
                    key: "signin",
                    data: "1"
                  })
                  wx.showToast({
                    title: '签到成功',
                    icon: 'success',
                    duration: 2000
                  })
                  setTimeout(function () {
                    wx.switchTab({
                      url: "../index/index"
                    })
                  }, 1000)
                } else {
                  wx.showToast({
                    title: '签到失败',
                    icon: 'none',
                    duration: 2000
                  })
                  setTimeout(function () {
                    wx.switchTab({
                      url: "../index/index"
                    })
                  }, 1000)
                }
              },
              fail: function (res) {
                wx.showToast({
                  title: '签到失败',
                  icon: 'none',
                  duration: 2000
                })
                setTimeout(function () {
                  wx.switchTab({
                    url: "../index/index"
                  })
                }, 1000)
              },
            })
          } else {
            that.scanSign()
          }
        } else {// 如果用户已经签到了，则直接显示签到成功，不需要再调用摄像头
          wx.showToast({
            title: '签到成功',
            icon: 'success',
            duration: 2000
          })
          setTimeout(function () {
            wx.switchTab({
              url: "../index/index"
            })
          }, 1000)
        }
      }
    })
  },

  // 扫码签到
  scanSign: function () {
    let that = this;
    let openid = wx.getStorageSync('openid')
    let session_key = wx.getStorageSync('session_key')
    
    wx.scanCode({
      success: (res) => {
        var temp = res.result.split('?') || 1
        var pass = temp[temp.length - 1] || 1
        temp = pass.split('=')
        pass = temp[temp.length - 1] || 1
        wx.request({
          url: Api.API_HOST + 'signin',
          data: {
            session_key: session_key,
            openid: openid,
            pass: pass
          },
          method: 'POST',
          success: function (res) {
            if (res.data.state) {
              wx.setStorage({
                key: "signin",
                data: "1"
              })
              wx.showToast({
                title: '签到成功',
                icon: 'success',
                duration: 2000
              })
              setTimeout(function () {
                wx.switchTab({
                  url: "../index/index"
                })
              }, 1000)
            } else {
              wx.showToast({
                title: '签到失败',
                icon: 'none',
                duration: 2000
              })
              setTimeout(function () {
                wx.switchTab({
                  url: "../index/index"
                })
              }, 1000)
            }
          },
          fail: function (res) {
            wx.showToast({
              title: '签到失败',
              icon: 'none',
              duration: 2000
            })
            setTimeout(function () {
              wx.switchTab({
                url: "../index/index"
              })
            }, 1000)
          },
        })
      }
    })
  }
})